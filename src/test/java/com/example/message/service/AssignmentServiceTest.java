package com.example.message.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.message.beans.BalanceUpdate;

/**
 * 
 * Test class for AssignmentService
 *
 * @see com.example.message.service.AssignmentService
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
public class AssignmentServiceTest {

	@Value("${application.filepath}")
	private String filepath;
	@Autowired
	private AssignmentService assignmentService;

	/**
	 * Test case with blank input.
	 * 
	 * {@link com.example.message.service.AssignmentService#mapToBalanceUpdate(String)}}
	 */
	@Test
	public void testMapToBalanceUpdateBlankInput() {
		BalanceUpdate balanceUpdate = assignmentService.mapToBalanceUpdate("");
		assertNull(balanceUpdate, "As input is blank null object should be returned");
	}

	/**
	 * Test case with valid input.
	 * 
	 * {@link com.example.message.service.AssignmentService#mapToBalanceUpdate(String)}}
	 */
	@Test
	public void testMapToBalanceUpdateValidInput() {
		BalanceUpdate balanceUpdate = assignmentService
				.mapToBalanceUpdate("4515.46,4510.86,4510.86,2019-11-02T11:11:11");
		assertNotNull(balanceUpdate);

	}

	/**
	 * Test case with null input.
	 * 
	 * {@link com.example.message.service.AssignmentService#mapToBalanceUpdate(String)}}
	 */
	@Test
	public void testMapToBalanceUpdateNull() {
		assertThrows(NullPointerException.class, () -> {
			assignmentService.mapToBalanceUpdate(null);
		});
	}
}
