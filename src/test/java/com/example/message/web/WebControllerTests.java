/**
 * 
 */
package com.example.message.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.message.beans.BalanceUpdate;
import com.example.message.beans.Payment;
import com.example.message.service.AssignmentService;

/**
 * @author 754134
 * 
 *         App : message
 *
 *         File : WebControllerTests.java
 *
 */
@SpringBootTest
@ExtendWith(SpringExtension.class)
public class WebControllerTests {

	@Value("${application.filepath}")
	private String filepath;
	@Autowired
	private AssignmentService assignmentService;

	@Test
	public void getPayments() {
		List<Payment> payments = null;
		payments = assignmentService.getPayments(filepath);

		assertNotNull(payments);
		assertEquals(3, payments.size());

	}

	@Test
	public void fileData() {
		List<BalanceUpdate> balanceUpdates = null;
		balanceUpdates = assignmentService.processInputFile(filepath);
		assertNotNull(balanceUpdates);

	}

}
